<?php
/**
 * Monitor minimum 100 most recent Game of Thrones channel and video comments via YouTube Data API and persists to database.
 *
 * The crawler runs in an infinite loop so that crawled comments in database are constantly refreshed.  Each loop iteration crawls all comment threads and replies belonging to specified YouTube channel and updates them to database. Upon completion of an iteration, previously crawled comments are deleted from database so that the database always maintains the latest comments.
 *
 * To run the crawler:
 * 0. Set up mySQL database and table.
 * 1. Configure top.secrets.php with your Google API key and database access info.
 * 2. Enable YouTube Data API access for your Google API key at the Google developers console.
 * 3. Execute "php crawler.php" at command console.
 * 
 * Documentation for YouTube API v3 is available at https://developers.google.com/youtube/v3/docs/
 *
 * @license MIT - see LICENSE file.
 */


/**
 * Extract relevant fields to be stored to database from comment API JSON response.
 * @param id Comment ID. 
 * @param threadId Thread ID is same as comment ID when comment is top-level.
 * @param json YouTube API JSON response.
 */
function parseComment($id, $threadId, $json) {
    $data = array();
    $snippet = $json['snippet'];
    $data['username'] = $snippet['authorDisplayName'];
    $data['message'] = $snippet['textDisplay'];
    $data['datetime'] = $snippet['publishedAt'];
    $data['timestamp'] = DateTime::createFromFormat('Y-m-d\TH:i:s+', $data['datetime'])->getTimestamp();
    $data['id'] = $id;
    $data['threadId'] = $threadId;
    return $data;
}

/**
 * Submit API request to retrieve maximum 100 top level comments (threads).
 * @param key Google API key.
 * @param channelId YouTube channel ID.
 * @param nextPageToken Next results page identifier
 * @param json The decoded YouTube API JSON response to be returned.
 * @return Next results page identifier if available, or null.
 * @see <a href="https://developers.google.com/youtube/v3/docs/commentThreads">https://developers.google.com/youtube/v3/docs/commentThreads</a>
 */
function fetchThread($key, $channelId, $nextPageToken, &$json) {
    // init youtube API url
    // change allThreadsRelatedToChannelId to channelId to crawl only discussion comments
    $url = "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet,replies&key=$key";
    $url .= "&allThreadsRelatedToChannelId=$channelId&maxResults=100";
    if ($nextPageToken) {
        $url .= "&textFormat=html&pageToken=$nextPageToken";
    }
    // fetch data
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $json = json_decode(curl_exec($ch), true);
    curl_close($ch);
    return array_key_exists('nextPageToken', $json)? $json['nextPageToken']: null;
}

/**
 * Submit API request to retrieve maximum 100 reply comments for a given thread.
 * @param key Google API key.
 * @param topLevelCommentId Thread ID.
 * @param nextPageToken Next results page identifier
 * @param json The decodeded API JSON response to be returned.
 * @return Next results page identifier if available, or null.
 * @see <a href="https://developers.google.com/youtube/v3/docs/comments">https://developers.google.com/youtube/v3/docs/comments</a>
 */
function fetchReplies($key, $topLevelCommentId, $nextPageToken, &$json) {
    // init youtube API url
    $url = "https://www.googleapis.com/youtube/v3/comments?part=snippet&key=$key";
    $url .= "&parentId=$topLevelCommentId&maxResults=100";
    if ($nextPageToken) {
        $url .= "&textFormat=html&pageToken=$nextPageToken";
    }
    // fetch data
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $json = json_decode(curl_exec($ch), true);
    curl_close($ch);
    return array_key_exists('nextPageToken', $json)? $json['nextPageToken']: null;
}

/**
 * Save comment to database.
 * @param servername Database host.
 * @param username Database user name
 * @param password Database user password.
 * @param dbname Database name.
 * @param crawlTimestamp Time of crawling in UNIX timestamp.
 * @param data The comment data to be saved.
 */
function persistDatabase($servername, $username, $password, $dbname, $crawlTimestamp, $data) {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // prepare sql and bind parameters
    $stmt = $conn->prepare("REPLACE INTO comments (crawl_timestamp, timestamp, uid, pid, username, message) VALUES (:crawl_timestamp, :timestamp, :uid, :pid, :username, :message)");
    $stmt->bindParam(':crawl_timestamp', $crawlTimestamp);
    $stmt->bindParam(':timestamp', $timestamp);
    $stmt->bindParam(':uid', $uid);
    $stmt->bindParam(':pid', $pid);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':message', $message);
    // insert a row
    $uid = $data['id'];
    $pid = $data['threadId'];
    $username = substr($data['username'], 0, 255);
    $message = $data['message'];
    $timestamp = $data['timestamp'];
    $crawlTimestamp = $crawlTimestamp;
    $stmt->execute();
    $conn = null;
}

/**
 * Delete previously crawled comments prior to given date from database.
 * @param servername Databse host name.
 * @param username Databse user name.
 * @param password Databse user password.
 * @param dbname Database name.
 * @param crawlTimestamp Crawling time in UNIX timestamp.
 */
function refreshDatabase($servername, $username, $password, $dbname, $crawlTimestamp) {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // prepare sql and bind parameters
    $query = $conn->prepare("DELETE FROM comments WHERE crawl_timestamp < :crawl_timestamp");
    $query->execute(array(":crawl_timestamp" => $crawlTimestamp));
    $conn = null;
}


require('top.secrets.php');

date_default_timezone_set('UTC');
$maxCount = 100; // min number of latest comments to monitor
$secondsInYear = 365 * 24 * 60 * 60;
while (true) {
    $crawledThreads = 0;
    $crawledReplies = 0;
    $crawlTimestamp = time(); // comments crawled in this loop iteration are timestamped
    $startTime = microtime(true);
    $nextCommentPageToken = null;
    do {
        $nextCommentPageToken = fetchThread($key, $channelId, $nextCommentPageToken, $threadJson);
        sleep(1); // respect api rate limiting
        // parse each thread and replies
        if (array_key_exists('items', $threadJson)) {
            foreach ($threadJson['items'] as $thread) {
                // parse thread
                $threadId = $thread['id'];
                $threadData = parseComment($threadId, $threadId, $thread['snippet']['topLevelComment']);
                $threadTimestamp = $threadData['timestamp'];
                // save thread to db
                persistDatabase($servername, $username, $password, $dbname, $crawlTimestamp, $threadData);
                $crawledThreads++;
                // parse replies
                $totalReplyCount = $thread['snippet']['totalReplyCount'];
                if ($totalReplyCount) {
                    $replies = $thread['replies']['comments'];
                    if (count($replies) == $totalReplyCount) {
                        // fetch comment replies from thread json
                        foreach ($replies as $reply) {
                            // save reply to db
                            persistDatabase($servername, $username, $password, $dbname, $crawlTimestamp, parseComment($reply['id'], $threadId, $reply));
                            $crawledReplies++;
                        }
                    }
                    else {
                        // fetch comment replies via api call
                        $nextRepliesPageToken = null;
                        do {
                            $nextRepliesPageToken = fetchReplies($key, $threadId, $nextRepliesPageToken, $repliesJson);
                            sleep(1); // respect api rate limiting
                            if (array_key_exists('items', $repliesJson)) {
                                foreach ($repliesJson['items'] as $reply) {
                                    // save reply to db
                                    persistDatabase($servername, $username, $password, $dbname, $crawlTimestamp, parseComment($reply['id'], $threadId, $reply));
                                    $crawledReplies++;
                                }
                            }
                        } while ($nextRepliesPageToken);
                    }
                }

                // output debugging info
                $elapsedTime = microtime(true) - $startTime;
                echo "thread time={$threadData['datetime']} total time={$elapsedTime}s threads crawled=$crawledThreads replies crawled=$crawledReplies\n";

                // threads older than 1 year have no comments so stop crawling if min comments quota is met
                if (time() - $threadTimestamp > $secondsInYear && $crawledThreads + $crawledReplies >= $maxCount) {
                    break 2;
                }
            }
        }
    } while ($nextCommentPageToken);

    // delete previously crawled comments from db
    refreshDatabase($servername, $username, $password, $dbname, $crawlTimestamp);

    // output debugging info
    $elapsedTime = microtime(true) - $startTime;
    echo "total time={$elapsedTime}s threads crawled=$crawledThreads replies crawled=$crawledReplies\n";
}

