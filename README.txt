Monitor YouTube channel comments by regularly crawling the latest comments via YouTube Data API.

The PHP crawler script runs in an infinite loop.  Each loop iteration crawls all comment threads and replies belonging to the given YouTube channel and updates to database.

To run it in background well after terminal logout:

    nohup php crawler.php >/dev/null 2>&1 &

To run it at boot, place it in /etc/init.d

A simple UI web page is provided to display crawled comments, publication date, commentator and crawl time.  Additional functionalities like pagination, sorting and searching are available on the UI page.  To access the web page, place the php scripts in your web server document root and go to http://<website_url>/index.php



SET UP DATABASE
----------------

* Created following database table to store comments.  The table and columns are:
 
    crawl_timestamp    Unix timestamp when comment was crawled from YouTube.
    timestamp          Unix timestamp of comment publication.
    uid                Comment ID assigned by YouTube.
    gid                Parent comment ID assigned by YouTube.  For top level comments, gid is same as uid.
    username           Commentator username.
    message            Comment text.

    CREATE TABLE comments (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `crawl_timestamp` INT UNSIGNED NOT NULL,
        `timestamp` INT UNSIGNED NOT NULL,
        `uid` VARCHAR(100) NOT NULL,
        `pid` VARCHAR(100),
        `username` VARCHAR(255) NOT NULL,
        `message` TEXT NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;


* Created following unique index on uid column to support "replace into" query.

    CREATE UNIQUE INDEX index_comments_uid ON comments (uid);



TODO
----

* Batch SQLs for speed.

* Implement error handling and logging in crawler.

* Exploit the etag from YouTube API response for caching and speed up.

* Generalize the database structure to support other resources besides YouTube.

