<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>YouTube Crawler</title>
        <link rel="stylesheet" href="css/all.css" type="text/css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#example').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "server_processing.php",
                    "order": [[ 0, "desc" ]],
                } );
            } );
        </script>
        <style>
            body {
                font-family: arial, courier;
                color: #333;
                font-size: 13px;
            }
        </style>
    </head>
    <body>
        <center><h2>Latest Game of Thrones YouTube Channel Comments</h2></center>

        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Publication Date</th>
                    <th>User</th>
                    <th>Comment</th>
                    <th>Crawl Time</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Publication Date</th>
                    <th>User</th>
                    <th>Comment</th>
                    <th>Crawl Time</th>
                </tr>
            </tfoot>
        </table>
    </body>
</html>

